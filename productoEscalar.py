from sympy import *
import math
#Calculo de producto escalar de dos vectores en R3
componenteX1 = sympify(input("X1: "))
componenteY1 = sympify(input("Y1: "))
componenteZ1 = sympify(input("Z1: "))
componenteX2 = sympify(input("X2: "))
componenteY2 = sympify(input("Y2: "))
componenteZ2 = sympify(input("Z2: "))
resultado = componenteX1*componenteX2+componenteY1*componenteY2+componenteZ1*componenteZ2
resultado = str(resultado)
componenteX1 = str(componenteX1)
componenteY1 = str(componenteY1)
componenteZ1 = str(componenteZ1)
componenteX2 = str(componenteX2)
componenteY2 = str(componenteY2)
componenteZ2 = str(componenteZ2)
print("#############------RESULTADO------#############")
print("         <("+componenteX1+", "+componenteY1+", "+componenteZ1+");("+componenteX2+", "+componenteY2+", "+componenteZ2+")> = "+resultado)
print("#############---------------------#############")

